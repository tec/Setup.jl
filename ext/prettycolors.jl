module prettycolors

using Colors
using LinearAlgebra # for the Adjoint type

import Base.show

colorcode(rgb::Vector{<:Integer}, background=false) =
    string("\e[", if background "48" else "38" end,
           ";2;", rgb[1], ";", rgb[2], ";", rgb[3], "m")

colorcode(c::Union{RGB, RGB24}, background=false) =
    colorcode(reinterpret.([red(c), green(c), blue(c)]), background)

colorcode(c::RGB{Float64}, background=false) =
    colorcode(round.(Int, 255 .* [red(c), green(c), blue(c)]), background)

colorcode(c::Colorant, background=false) =
    colorcode(convert(RGB24, c), background)

function show(io::IO, ::MIME"text/plain", c::Colorant)
    if get(io, :color, false) && get(ENV, "COLORTERM", nothing) == "truecolor"
        print(colorcode(c, true), "  \e[0m ")
    end
    show(io, c)
end

function show(io::IO, ::MIME"text/plain", cs::Adjoint{<:Colorant, <:Vector{<:Colorant}})
    summary(io, cs)
    print(":\n")
    if get(io, :color, false) && get(ENV, "COLORTERM", nothing) == "truecolor"
        print(" ", join(colorcode.(cs,true), if length(cs) <= 40 "  " else " " end),
              if length(cs) <= 40 "  \e[0m" else " \e[0m" end)
    else
        Base.print_array(IOContext(io, :SHOWN_SET => cs), cs)
    end
end

function show(io::IO, ::MIME"text/plain", cs::Matrix{<:Colorant})
    summary(io, cs)
    print(":\n")
    if get(io, :color, false) && *(size(cs)...) <= 1000 && get(ENV, "COLORTERM", nothing) == "truecolor"
        for csrow in eachrow(cs)
            println(" ", join(colorcode.(csrow,true),
                              if size(cs,2) <= 40 "  " else " " end),
                    if size(cs,2) <= 40 "  \e[0m" else " \e[0m" end)
        end
    else
        Base.print_array(IOContext(io, :SHOWN_SET => cs), cs)
    end
end

function Base.display(c::Colorant)
    show(stdout, MIME("text/plain"), c)
    print('\n')
end

end
