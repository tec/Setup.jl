# This is loaded by all other `image_*.jl` files

using Setup

const IMAGE_SHOW_PKG, IMAGE_IO_PKG, IMAGE_MAGICK_PKG = splat(Base.PkgId).([
    (Base.UUID("4e3cecfd-b093-5904-9786-8bbb286a6a31"), "ImageShow"),
    (Base.UUID("82e4d734-157c-48bb-816b-45c225c6df19"), "ImageIO"),
    (Base.UUID("6218d12a-5da1-5696-b52f-db25d2ecc6d1"), "ImageMagick")])

function __init__()
    if isinteractive()
        Setup.load_termimage_pkg()
        if !haskey(Base.loaded_modules, IMAGE_SHOW_PKG) &&
            !isnothing(Base.find_package(IMAGE_SHOW_PKG.name)) &&
            (!isnothing(Base.find_package(IMAGE_IO_PKG.name)) ||
             !isnothing(Base.find_package(IMAGE_MAGICK_PKG.name)))
            # Don't to this async because it may affect a `display` call
            # that's just about to occur.
            if !isnothing(Base.find_package(IMAGE_IO_PKG.name))
                Base.require(IMAGE_IO_PKG)
            elseif !isnothing(Base.find_package(IMAGE_MAGICK_PKG.name))
                Base.require(IMAGE_MAGICK_PKG)
            else
                return
            end
            Base.require(IMAGE_SHOW_PKG)
        end
    end
end
