module ohmyrepl

using Setup
using OhMyREPL

function __init__()
    theme = Setup.termtheme()
    if theme == :dark
        OhMyREPL.colorscheme!("OneDark")
    elseif theme == :light
        OhMyREPL.colorscheme!("TomorrowDay")
    else theme == :unknown
        OhMyREPL.colorscheme!("Monokai16")
    end

    if Setup.TERM[] == "xterm-kitty"
        OhMyREPL.input_prompt!(:green) do
            print("\e]133;A\e\\")
            "julia> "
        end
        OhMyREPL.output_prompt!(:red) do
            print("\e]133;C\e\\")
            ""
        end
    end
end

end
