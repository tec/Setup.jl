module cairomakie

using Setup
using CairoMakie

import Setup.theme_term

import CairoMakie.Colors.FixedPointNumbers.N0f8
import CairoMakie.Colors.RGB

import CairoMakie.Makie: Theme, RGBAf, Attributes, Axis, Axis3, Legend, Colorbar

function theme_term()
    termcolors = Setup.termcolors()

    if isempty(termcolors)
        return CairoMakie.Makie.current_default_theme()
    end

    function tocolor(name::Symbol)
        rgb = get(termcolors, name, (r=0x00, g=0x00, b=0x00))
        RGB(reinterpret(N0f8, rgb.r),
            reinterpret(N0f8, rgb.g),
            reinterpret(N0f8, rgb.b))
    end

    bg, fg, red, green, yellow, blue, magenta, cyan,
    lbg, lfg, lred, lgreen, lyellow, lblue, lmagenta, lcyan =
        tocolor.((:black, :white, :red, :green, :yellow, :blue,
                :magenta, :cyan, :light_black, :light_white,
                :light_red, :light_green, :light_yellow,
                :light_blue, :light_magenta, :light_cyan))

    onecolors(α = 1.0) =
        RGBAf.([blue, yellow, green, magenta, red, cyan, lblue,
                lyellow, lgreen, lmagenta, lred, lcyan],
            α)

    Theme(
        backgroundcolor = bg,
        textcolor = fg,
        linecolor = fg,
        font = :regular,
        fonts = Attributes(
            bold = "JetBrains Mono Bold",
            bold_italic = "JetBrains Mono Bold Italic",
            italic = "JetBrains Mono Italic",
            regular = "JetBrains Mono Medium"),
        palette = merge(Attributes(
            color = onecolors(1.0),
            patchcolor = onecolors(0.8)
        ), CairoMakie.Makie.DEFAULT_PALETTES),
        Axis = (
            backgroundcolor = :transparent,
            bottomspinecolor = fg,
            topspinecolor = fg,
            leftspinecolor = fg,
            rightspinecolor = fg,
            xgridcolor = RGBAf(1, 1, 1, 0.16),
            ygridcolor = RGBAf(1, 1, 1, 0.16),
            xtickcolor = fg,
            ytickcolor = fg),
        Legend = (
            framecolor = fg,
            bgcolor = bg),
        Axis3 = (
            xgridcolor = RGBAf(1, 1, 1, 0.16),
            ygridcolor = RGBAf(1, 1, 1, 0.16),
            zgridcolor = RGBAf(1, 1, 1, 0.16),
            xspinecolor_1 = fg,
            yspinecolor_1 = fg,
            zspinecolor_1 = fg,
            xspinecolor_2 = fg,
            yspinecolor_2 = fg,
            zspinecolor_2 = fg,
            xspinecolor_3 = fg,
            yspinecolor_3 = fg,
            zspinecolor_3 = fg,
            xticklabelpad = 3,
            yticklabelpad = 3,
            zticklabelpad = 6,
            xtickcolor = fg,
            ytickcolor = fg,
            ztickcolor = fg),
        Colorbar = (
            tickcolor = fg,
            spinecolor = fg,
            topspinecolor = fg,
            bottomspinecolor = fg,
            leftspinecolor = fg,
            rightspinecolor = fg))
end

function __init__()
    if isinteractive()
        Setup.load_termimage_pkg()
        CairoMakie.set_theme!(theme_term())
    end
end

end
