module hdf5

using HDF5

import HDF5._tree_icon

function __init__()
    _tree_icon(::Type{HDF5.Attribute}) =
        HDF5.SHOW_TREE_ICONS[] ? "\e[33m\e[39m" : "[A]"
    _tree_icon(::Type{HDF5.Group}) =
        HDF5.SHOW_TREE_ICONS[] ? "\e[34m\e[39m" : "[G]"
    _tree_icon(::Type{HDF5.Dataset}) =
        HDF5.SHOW_TREE_ICONS[] ? "\e[35m\e[39m" : "[D]"
    _tree_icon(::Type{HDF5.Datatype}) =
        HDF5.SHOW_TREE_ICONS[] ? "\e[36m\e[39m" : "[T]"
    _tree_icon(::Type{HDF5.File}) =
        HDF5.SHOW_TREE_ICONS[] ? "\e[34m\e[39m" : "[F]"
    _tree_icon(::Type) =
        HDF5.SHOW_TREE_ICONS[] ? "\e[31m❓\e[39m" : "[?]"
end

end
