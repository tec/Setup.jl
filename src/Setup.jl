module Setup

import REPL

export about, @about

@static if VERSION >= v"1.8"
    TERM::Ref{String} = get(ENV, "TERM", "")
    IS_SSH::Ref{Bool} = Ref(false)
else
    TERM = get(ENV, "TERM", "")
    IS_SSH = Ref(false)
end

include("install.jl")

include("cmdpuns.jl")
include("termsetup.jl")
include("about.jl")

include("pkgutils.jl")

include("autoloads.jl")
using .Autoloads
include("autoload_data.jl")

include("sessions.jl")
using .Sessions

function theme_term end # For cairomakie.jl

function __init__()
    IS_SSH[] = haskey(ENV, "SSH_CLIENT") || haskey(ENV, "SSH_TTY")
    if isinteractive()
        TERM[] = termcode()

        Main.Threads.@spawn if TERM[] == "xterm-kitty"
            termtitle(pwd())
        end

        if ENV["TERM"] != "dumb"
            replsetup()
        end
    end
end

precompile(termtitle, ())
precompile(replsetup, ())
precompile(_termcolors, ())
precompile(termcolors, ())
precompile(termtheme, ())
precompile(__init__, ())

precompile(Autoloads.autoload_loadpkg, (Symbol,))
precompile(Autoloads.autoload_scan!, (Expr,))
precompile(Autoloads.autoload_scan!, (Symbol,))
precompile(Autoloads.autoload_scan!, (Any,))
precompile(Autoloads.read_named_envs!, ())
precompile(Autoloads.repl_scan_autoloads!, (Expr,))
precompile(Autoloads.repl_scan_autoloads!, (Nothing,))
precompile(Autoloads.__init__, ())

end
